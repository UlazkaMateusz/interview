1. What is .Net?

.NET is a free and open-source application platform supported by Microsoft. 
.NET is a secure, reliable, and high-performance application platform.

C# is the programming language for .NET. It's strongly-typed and type-safe and has integrated concurrency and automatic memory management.

2. Initialization vs declaration

The term declaration means to declare the creation of variables and functions.
Declaration is not concerned with the storage (initialization) of values. Instead, its job is to declare the creation of variables and functions.
```javascript
var color;
let length;
```

The term initialization means to assign an initial value to a variable.
```javascript
let number = 5;
const color = "green";
```

3. Invocation

The term invocation means to invoke a piece of code.
```javascript
var color = "green";

// Invoke the color variable:
console.log(color);

// The invocation above will return:
"green"
```

In summary
- Declaration declares the creation of variables and functions.
- Initialization assigns initial values to variables.
- Invocation executes a piece of code.

4. What is var in C#?

When you declare a local variable, you can let the compiler infer the type of the variable from the initialization expression. To do that use the var keyword instead of the name of a type:
```C#
var greeting = "Hello";
Console.WriteLine(greeting.GetType());  // output: System.String

var a = 32;
Console.WriteLine(a.GetType());  // output: System.Int32

var xs = new List<double>();
Console.WriteLine(xs.GetType());  // output: System.Collections.Generic.List`1[System.Double]
```

5. Value type vs Reference type

Variables of reference types store references to their data (objects), while variables of value types directly contain their data. With reference types, two variables can reference the same object; therefore, operations on one variable can affect the object referenced by the other variable. With value types, each variable has its own copy of the data, and it's not possible for operations on one variable to affect the other (except in the case of in, ref, and out parameter variables; see in, ref, and out parameter modifier).


The following keywords are used to declare reference types:
- class
- interface
- delegate
- record


C# also provides the following built-in reference types:
- dynamic
- object
- string

A value type can be one of the two following kinds:
- struct
- enum

A nullable value type T? represents all values of its underlying value type T and an additional null value. You cannot assign null to a variable of a value type, unless it's a nullable value type.

6. Managed vs unmanaged memory

The Microsoft definition is that managed memory is cleaned up by a Garbage Collector (GC), i.e. some process that periodically determines what part of the physical memory is in use and what is not.

Unmanaged memory is cleaned up by something else e.g. your program or the operating system.

7. IDisposable

Provides a mechanism for releasing unmanaged resources.

If your app simply uses an object that implements the IDisposable interface, you should call the object's IDisposable.Dispose implementation when you are finished using it.

You should implement IDisposable if your type uses unmanaged resources directly or if you wish to use disposable resources yourself. The consumers of your type can call your IDisposable.Dispose implementation to free resources when the instance is no longer needed.

The using statement ensures the correct use of an IDisposable instance:
```C#
var numbers = new List<int>();
using (StreamReader reader = File.OpenText("numbers.txt"))
{
    string line;
    while ((line = reader.ReadLine()) is not null)
    {
        if (int.TryParse(line, out int number))
        {
            numbers.Add(number);
        }
    }
}
// Here reader is disposed
```
In this example reader will be disposed when it's out of scope.

The language will allow for using to be added to a local variable declaration. Such a declaration will have the same effect as declaring the variable in a using statement at the same location.
```
public void MyFunc() {
    using var fileStream = new FileStream("...");
    // code using fileStream  should be here
}
```
Variable fileStream will be disposed when it's out of scope. In this example this will happend on the end of function MyFunc()


8. IAsyncDisposable

It's almost the same as IDisposable but you need to implememnt method DisposeAsync

### Caution
If you implement the IAsyncDisposable interface but not the IDisposable interface, your app can potentially leak resources. If a class implements IAsyncDisposable, but not IDisposable, and a consumer only calls Dispose, your implementation would never call DisposeAsync. This would result in a resource leak.

You can use keyword await using with IAsyncDisposable
```C#
await using (var resource = new AsyncDisposableExample())
{
    // Use the resource
}
```

```C#
public Task MyFunc() {
    await using var fileStream = new FileStream("...");
    // code using fileStream  should be here
}
```

9. Disposable pattern
[Documentation](https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/dispose-pattern)


10. Nullable type

A nullable value type T? represents all values of its underlying value type T and an additional null value. For example, you can assign any of the following three values to a bool? variable: true, false, or null. An underlying value type T cannot be a nullable value type itself (it can be nullable, but then ? does not do anything).

Any nullable value type is an instance of the generic System.Nullable<T> structure. You can refer to a nullable value type with an underlying type T in any of the following interchangeable forms: Nullable<T> or T?.

```C#
double? pi = 3.14;
char? letter = 'a';

int m2 = 10;
int? m = m2;

bool? flag = null;

// An array of a nullable value type:
int?[] arr = new int?[10];
```

Implementation of Nullable type can be found [here](https://github.com/microsoft/referencesource/blob/master/mscorlib/system/nullable.cs). It's simple and worh reading.


11. Large object heap light read

It's a longer topic so I provided some usefull links.
[Stack overflow](https://stackoverflow.com/questions/8951836/why-large-object-heap-and-why-do-we-care)
[Medium article](https://medium.com/@saravanan.saaye/what-is-the-large-object-heap-loh-in-net-core-c97ac47f3c11)
[Microsoft documentation](https://learn.microsoft.com/en-us/dotnet/standard/garbage-collection/large-object-heap)

12. Garbage collection in C#

[Microsoft documentation](https://learn.microsoft.com/en-us/dotnet/standard/garbage-collection/fundamentals)

13. Immutable vs mutable 

The meaning of these words is the same in C# programming language; that means the mutable types are those whose data members can be changed after the instance is created but Immutable types are those whose data members can not be changed after the instance is created.

For the record:
- String is the standard C# / .Net immutable string type
- StringBuilder is the standard C# / .Net mutable string type

To "effect a change" on a string represented as a C# String, you actually create a new String object. The original String is not changed ... because it is unchangeable.

In most cases it is better to use String because it is easier reason about them; e.g. you don't need to consider the possibility that some other thread might "change my string".

However, when you need to construct or modify a string using a sequence of operations, it may be more efficient to use a StringBuilder. An example is when you are concatenating many string fragments to form a large string:

- If you do this as a sequence of String concatenations, you copy O(N^2) characters, where N is the number of component strings.
- If use a StringBuilder you only copy O(N) characters.

14. Yield return

You use the yield statement in an iterator to provide the next value or signal the end of an iteration. The yield statement has the two following forms:

yield return: to provide the next value in iteration, as the following example shows:
```C#
foreach (int i in ProduceEvenNumbers(9))
{
    Console.Write(i);
    Console.Write(" ");
}
// Output: 0 2 4 6 8

IEnumerable<int> ProduceEvenNumbers(int upto)
{
    for (int i = 0; i <= upto; i += 2)
    {
        yield return i;
    }
}
```

yield break: to explicitly signal the end of iteration, as the following example shows:
```C#
Console.WriteLine(string.Join(" ", TakeWhilePositive(new int[] {2, 3, 4, 5, -1, 3, 4})));
// Output: 2 3 4 5

Console.WriteLine(string.Join(" ", TakeWhilePositive(new int[] {9, 8, 7})));
// Output: 9 8 7

IEnumerable<int> TakeWhilePositive(IEnumerable<int> numbers)
{
    foreach (int n in numbers)
    {
        if (n > 0)
        {
            yield return n;
        }
        else
        {
            yield break;
        }
    }
}
```

15. Attribiutes and reflections

[Microsoft documentation](https://learn.microsoft.com/en-us/dotnet/csharp/advanced-topics/reflection-and-attributes/)

16. Authentication and Authorization

Authentication (AuthN) is a process that verifies that someone or something is who they say they are. Technology systems typically use some form of authentication to secure access to an application or its data. For example, when you need to access an online site or service, you usually have to enter your username and password. Then, behind the scenes, it compares the username and password you entered with a record it has on its database.

Authorization is the security process that determines a user or service's level of access. In technology, we use authorization to give users or services permission to access some data or perform a particular action.

17. Destructors in C#

Destructors in C# are methods inside the class used to destroy instances of that class when they are no longer needed. The Destructor is called implicitly by the .NET Framework’s Garbage collector and therefore programmer has no control as when to invoke the destructor. An instance variable or an object is eligible for destruction when it is no longer reachable.

Important Points:
- A Destructor is unique to its class i.e. there cannot be more than one destructor in a class.
- A Destructor has no return type and has exactly the same name as the class name (Including the same case).
- It is distinguished apart from a constructor because of the Tilde symbol (~) prefixed to its name.
- A Destructor does not accept any parameters and modifiers.
- It cannot be defined in Structures. It is only used with classes.
- It cannot be overloaded or inherited.
- It is called when the program exits.
- Internally, Destructor called the Finalize method on the base class of object.

```C#
class Example
{ 
    // Rest of the class
    // members and methods.

   // Destructor
   ~Example()
    {
        // Your code
    }

} 
```

18. Primary constructors in .C#

[Microsoft Documentation](https://learn.microsoft.com/en-us/dotnet/csharp/whats-new/tutorials/primary-constructors)

19. Restful methods

[Article on all methods](https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/HTTP-methods)

20. HATEOAS

[Code maze example](https://code-maze.com/hateoas-aspnet-core-web-api/)

21. Playload in HTTP

The Request Payload - or to be more precise: payload body of a HTTP Request

- is the data normally send by a POST or PUT Request. It's the part after the headers and the CRLF of a HTTP Request.

A request with Content-Type: application/json may look like this:

```
POST /some-path HTTP/1.1
Content-Type: application/json

{ "foo" : "bar", "name" : "John" }

```

If you submit this per AJAX the browser simply shows you what it is submitting as payload body. That’s all it can do because it has no idea where the data is coming from.

If you submit a HTML-Form with method="POST" and Content-Type: application/x-www-form-urlencoded or Content-Type: multipart/form-data your request may look like this:

```
POST /some-path HTTP/1.1
Content-Type: application/x-www-form-urlencoded

foo=bar&name=John
```

In this case the form-data is the request payload. Here the Browser knows more: it knows that bar is the value of the input-field foo of the submitted form. And that’s what it is showing to you.

So, they differ in the Content-Type but not in the way data is submitted. In both cases the data is in the message-body. And Chrome distinguishes how the data is presented to you in the Developer Tools.

22. Rate limiting

[Cloudflare article](https://www.cloudflare.com/learning/bots/what-is-rate-limiting/)

23. Volatile 

[Microsoft Documentation](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/volatile)

24. Async Await

[Microsoft Documentation](https://learn.microsoft.com/en-us/dotnet/csharp/asynchronous-programming/async-scenarios)
