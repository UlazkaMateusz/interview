1. Entity (Encja)

2. Table
3. Procedure

4. Inheritance in entity framework
[Microsoft Documentation](https://learn.microsoft.com/en-us/ef/core/modeling/inheritance)

5. Materialized View
6. Materialized View Pattern
[Microsoft Documentation](https://learn.microsoft.com/en-us/azure/architecture/patterns/materialized-view)
7. Transactions
8. Database romalization
[Article](https://www.freecodecamp.org/news/database-normalization-1nf-2nf-3nf-table-examples/)
9. ACID
10. Indexes
